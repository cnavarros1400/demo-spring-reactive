package com.luden.springboot.webflux;

import com.luden.springboot.webflux.model.document.Category;
import com.luden.springboot.webflux.model.document.Product;
import com.luden.springboot.webflux.repository.ProductRepository;
import com.luden.springboot.webflux.service.CategoryService;
import com.luden.springboot.webflux.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import reactor.core.publisher.Flux;
import java.util.*;

@Slf4j
@RequiredArgsConstructor
@SpringBootApplication
public class SpringBootWebfluxApplication implements CommandLineRunner {

	private final ProductService productService;
	private final CategoryService categoryService;
	private final ReactiveMongoTemplate reactiveMongoTemplate;

	public static void main(String[] args) {
		SpringApplication.run(SpringBootWebfluxApplication.class, args);
	}

	@Override
	public void run(String... args){
		reactiveMongoTemplate.dropCollection("product").subscribe();
		reactiveMongoTemplate.dropCollection("category").subscribe();

		int productQuantity = 10;
		String productName = "product ";
		Random rd = new Random();

		List<Category> categories = new ArrayList<>();
		for(int i = 1; i<=5; i++){
			categories.add(Category.builder().name("Category ".concat(String.valueOf(i))).build());
		}

		List<Product> products = new ArrayList<>();
		for(int i = 1; i<= productQuantity; i++){
			products.add(Product.builder().name(productName.concat(String.valueOf(i))).price(rd.nextDouble(100)).category(categories.get(rd.nextInt(1,5))).build());
		}
		Flux.fromIterable(categories)
						.flatMap(categoryService::save)
				.doOnNext(category -> log.info("Category saved in MongoDB: ".concat(category.toString())))
				.thenMany(Flux.fromIterable(products)
								.flatMap(product -> {
									product.setCreateAt(new Date());
									return productService.save(product);
								})
						//flatmap converts from a type to another different, in this case: Mono<Product> to Product
				)
				.subscribe(productMono -> log.info("Inserted into mongo: ".concat(productMono.toString())));

	}

}
