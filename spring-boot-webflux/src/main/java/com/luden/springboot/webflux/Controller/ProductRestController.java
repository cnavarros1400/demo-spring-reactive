package com.luden.springboot.webflux.Controller;

import com.luden.springboot.webflux.model.document.Product;
import com.luden.springboot.webflux.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequestMapping("/api/product")
@RestController
@Slf4j
@RequiredArgsConstructor
public class ProductRestController {

    private final ProductService productService;

    @GetMapping()
    public Flux<Product> index(){
        return productService.findAll().map(product -> {
            product.setName(product.getName().toUpperCase());
            return product;
        })
                .doOnNext(product -> log.info(product.toString()));
    }

    @GetMapping("/{id}")
    public Mono<Product> findProductById(@PathVariable String id){
        //Mono<Product> product = productRepository.findById(id);
        //Alternative way, not recommended, just for practice
        Flux<Product> products = productService.findAll();
        Mono<Product> product = products.filter(p -> p.getId().equals(id))
                .next()
                        .doOnNext(p -> log.info(p.toString()));
        return  product;
    }

}
