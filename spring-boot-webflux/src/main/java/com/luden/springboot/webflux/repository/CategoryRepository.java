package com.luden.springboot.webflux.repository;

import com.luden.springboot.webflux.model.document.Category;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface CategoryRepository extends ReactiveMongoRepository<Category, String> {
}
