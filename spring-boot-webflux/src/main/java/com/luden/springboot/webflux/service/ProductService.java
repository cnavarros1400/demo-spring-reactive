package com.luden.springboot.webflux.service;

import com.luden.springboot.webflux.model.document.Product;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ProductService {

    Flux<Product> findAll();
    Flux<Product> findAllNamesUppercase();
    Flux<Product> findAllNamesUppercaseRepeat();

    Mono<Product> findById(String id);

    Mono<Product> save (Product product);

    Mono<Void> delete(Product product);

}
