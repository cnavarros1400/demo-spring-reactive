package com.luden.springboot.webflux.Controller;

import com.luden.springboot.webflux.model.document.Category;
import com.luden.springboot.webflux.model.document.Product;
import com.luden.springboot.webflux.service.CategoryService;
import com.luden.springboot.webflux.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.thymeleaf.spring5.context.webflux.ReactiveDataDriverContextVariable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.io.File;
import java.time.Duration;
import java.util.Date;
import java.util.UUID;

@SuppressWarnings("ReactiveStreamsUnusedPublisher")
@Controller
@RequiredArgsConstructor
@Slf4j
@SessionAttributes("product")
public class ProductController {

    @Value("${config.uploads.path}")
    private String uploadsPath;

    private final ProductService productService;
    private final CategoryService categoryService;

    @ModelAttribute("categories")
    public Flux<Category> categories(){
        return categoryService.findAll();
    }

    @GetMapping({"list", "/"})
    public Mono<String> list(Model model){
        Flux<Product> productsList = productService.findAllNamesUppercase();
        //productsList.subscribe(product -> {log.info(product.toString());});//ViewResolver automatically subscribes to this publisher
        model.addAttribute("productsList", productsList);
        model.addAttribute("title" ,"Product list");
        return Mono.just("productsList");
    }

    /**
     *
     * @return products list handling backpressure using Data driver
     */
    @GetMapping("data-driver-list")
    public String DataDriverList(Model model){
        Flux<Product> productsList = productService.findAllNamesUppercase()
                .delayElements(Duration.ofSeconds(1));
        //productsList.subscribe(product -> {log.info(product.toString());});//ViewResolver automatically subscribes to this publisher
        model.addAttribute("productsList", new ReactiveDataDriverContextVariable(productsList,1));
        model.addAttribute("title" ,"Product list");
        return "productsList";
    }

    /**
     *
     * @return products list handling backpressure using Chunk byte
     */
    @SuppressWarnings("DuplicatedCode")
    @GetMapping("full-chunked-list")
    public String FullChunkedList(Model model){
        Flux<Product> productsList = productService.findAllNamesUppercaseRepeat();
        //productsList.subscribe(product -> {log.info(product.toString());});
        model.addAttribute("productsList", productsList);
        model.addAttribute("title" ,"Product list");
        return "productsList";
    }

    /**
     *
     * @return products list handling backpressure using Chunk byte applied to specific views
     */
    @SuppressWarnings("DuplicatedCode")
    @GetMapping("view-chunked-list")
    public String ViewChunkedList(Model model){
        Flux<Product> productsList = productService.findAllNamesUppercaseRepeat();
        //productsList.subscribe(product -> {log.info(product.toString());});//ViewResolver automatically subscribes to this publisher
        model.addAttribute("productsList", productsList);
        model.addAttribute("title" ,"Product list");
        return "productsListViewChunked";
    }


    @GetMapping("/form")
    public Mono<String> create(Model model){
        model.addAttribute("product", new Product());
        model.addAttribute("title", "Product form");
        model.addAttribute("button", "Create");
        return Mono.just("form");
    }

    @PostMapping("/form")
    public Mono<String> save(@Valid Product product, BindingResult bindingResult, SessionStatus sessionStatus, Model model,@RequestPart FilePart file) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("product", new Product());
            model.addAttribute("title", "Error in fields");
            model.addAttribute("button", "Save");
            return Mono.just("form");
        } else {
            sessionStatus.setComplete();
            Mono<Category> category = categoryService.findBtId(product.getCategory().getId());
             return category.flatMap(c -> {
                         if (product.getCreateAt() == null) {
                             product.setCreateAt(new Date());
                         }
                         if(!file.filename().isEmpty()){
                            product.setFile(UUID.randomUUID().toString().concat("_").concat(file.filename()
                                    .replace(" ","")
                                    .replace(":","").replace("\\","")));
                         }
                        product.setCategory(c);
                        return productService.save(product);
                    })
                    .doOnNext(p -> log.info("Object to persist on mongodb: ".concat(product.toString())))
                    //        .thenReturn("redirect:/list");//alternative to return redirect value
                     .flatMap(p -> {
                         if(!file.filename().isEmpty()){
                             return file.transferTo(new File(uploadsPath.concat(p.getFile())));
                         }
                         return Mono.empty();
                     })
                    .then(Mono.just("redirect:/list?success=product+registered+successfully"));
        }
    }

    @GetMapping("/form/{id}")
    public Mono<String> edit(@PathVariable String id, Model model, SessionStatus sessionStatus){
        Mono<Product> product = productService.findById(id)
                .doOnNext(p -> log.info("Product from DB: ".concat(p.toString())))
                .defaultIfEmpty(new Product());
        model.addAttribute("product", product);
        model.addAttribute("title", "Modify data product");
        model.addAttribute("button", "Modify");
        return Mono.just("form");
    }

    /**
     * In this method is not applicable the sessionStorage of variables.
     * The sessionStorage is into the controller thread, different to the actual flux thread
     */
    @GetMapping("/form-v2/{id}")
    public Mono<String> editV2(@PathVariable String id, Model model, SessionStatus sessionStatus){
        return productService.findById(id)
                .doOnNext(p -> {
                    log.info("Product from DB: ".concat(p.toString()));
                    model.addAttribute("product", p);
                    model.addAttribute("title", "Modify data product");
                    model.addAttribute("button", "Modify");
                })
                .defaultIfEmpty(new Product())
                .flatMap(product -> {
                    if (product.getId() == null){
                        return Mono.error(new InterruptedException("Product does not exist..."));
                    }
                    return Mono.just(product);
                })
                .then(Mono.just("form"))
                .onErrorResume(ex -> Mono.just("redirect:/list?error=product+does+not+exist"));
    }

    @GetMapping("/delete/{id}")
    public Mono<String> delete(@PathVariable String id){
        return productService.findById(id)
                .defaultIfEmpty(new Product())
                .flatMap(product -> {
                    if (product.getId() == null){
                        return Mono.error(new InterruptedException("Product does not exist..."));
                    }
                    return Mono.just(product);
                })
                .flatMap(productService::delete).then(Mono.just("redirect:/list?success=product+deleted+successfully"))
                .onErrorResume(ex -> Mono.just("redirect:/list?error=product+does+not+exist"));
    }

}
