package com.luden.api.restfull.handler;

import com.luden.api.restfull.model.document.Category;
import com.luden.api.restfull.model.document.Product;
import com.luden.api.restfull.service.CategoryService;
import com.luden.api.restfull.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.http.codec.multipart.FormFieldPart;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.File;
import java.net.URI;
import java.util.*;

@Component
@RequiredArgsConstructor
@Slf4j
public class ProductHandler {

    private final ProductService productService;
    private final CategoryService categoryService;
    private final Validator validator;

    @Value("${config.uploads.path}")
    private String pathImage;

    public Mono<ServerResponse> list(ServerRequest serverRequest) {
        log.info("Begin - ReactiveEndPoint");
        return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(productService.findAll(), Product.class)
                .doOnTerminate(() -> log.info("End - ReactiveEndPoint - ok"));
    }

    public Mono<ServerResponse> detail(ServerRequest serverRequest) {
        log.info("Begin - ReactiveEndPoint");
        String id = serverRequest.pathVariable("id");
        return productService.findById(id)
                .flatMap(product -> ServerResponse
                        .ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(Mono.just(product), Product.class)
                        .doOnTerminate(() -> log.info("End - ReactiveEndPoint - ok")))
                .switchIfEmpty(ServerResponse.notFound().build().doOnTerminate(() -> log.info("End - ReactiveEndPoint - not found")));
    }

    public Mono<ServerResponse> create(ServerRequest serverRequest) {
        log.info("Begin - ReactiveEndPoint");
        Mono<Product> productMono = serverRequest.bodyToMono(Product.class);
        return productMono.flatMap(product -> {
            Errors errors = new BeanPropertyBindingResult(product, Product.class.getName());
            validator.validate(product, errors);
            if (errors.hasErrors()) {
                return Flux.fromIterable(errors.getFieldErrors())
                        .map(fieldError -> "Error on field '".concat(fieldError.getField()).concat("'. ").concat(Objects.requireNonNull(fieldError.getDefaultMessage())))
                        .collectList()
                        .flatMap(errorsList -> ServerResponse.badRequest().body(BodyInserters.fromValue(errorsList)))
                        .doOnTerminate(() -> log.info("End - ReactiveEndPoint - Field errors"));
            } else {
                return categoryService.findBtId(product.getCategory().getId())
                        .flatMap(category -> {
                            if (Objects.equals(category.getName(), product.getCategory().getName())) {
                                return productService.save(product)
                                        .flatMap(savedProduct -> ServerResponse.created(URI.create("/api/v2/product/".concat(savedProduct.getId())))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .body(Mono.just(savedProduct), Product.class))
                                        .doOnTerminate(() -> log.info("End - ReactiveEndPoint - ok"));
                            } else {
                                return ServerResponse.badRequest().body(BodyInserters.fromValue("Category id does not match with its name"))
                                        .doOnTerminate(() -> log.info("End - ReactiveEndPoint - category id does not match with name"));
                            }
                        })
                        .switchIfEmpty(ServerResponse.notFound().build().doOnTerminate(() -> log.info("End - ReactiveEndPoint - Category not found")));
            }
        });
    }

    public Mono<ServerResponse> modify(ServerRequest serverRequest) {
        log.info("Begin - ReactiveEndPoint");
        String id = serverRequest.pathVariable("id");
        Map<String, Object> response = new HashMap<>();
        Mono<Product> productRequest = serverRequest.bodyToMono(Product.class);
        Mono<Product> productDataBase = productService.findById(id);
        return productDataBase
                .flatMap(product -> {
                    
                })
                .switchIfEmpty(ServerResponse.notFound().build().doOnTerminate(() -> log.info("End - ReactiveEndPoint - not found")));

    }

    public Mono<ServerResponse> delete(ServerRequest serverRequest) {
        log.info("Begin - ReactiveEndPoint");
        String id = serverRequest.pathVariable("id");
        Mono<Product> productDB = productService.findById(id);
        return productDB.flatMap(product -> productService.delete(product).doOnTerminate(() -> log.info("End - ReactiveEndPoint - ok"))
                        .then(ServerResponse.noContent().build()))//using then: .delete returns a void so cannot use switchIfEmpty to handle errors
                .switchIfEmpty(ServerResponse.notFound().build().doOnTerminate(() -> log.info("End - ReactiveEndPoint - not found")));
    }

    public Mono<ServerResponse> uploadImage(ServerRequest serverRequest) {
        log.info("Begin - ReactiveEndPoint");
        String id = serverRequest.pathVariable("id");
        return serverRequest.multipartData()
                .map(stringPartMultiValueMap -> stringPartMultiValueMap.toSingleValueMap().get("image-file"))
                .cast(FilePart.class)
                .flatMap(filePart -> productService.findById(id)
                        .flatMap(product -> {
                            product.setFile(UUID.randomUUID().toString().concat("_").concat(filePart.filename())
                                    .replace(" ", "_")
                                    .replace(":", "")
                                    .replace("\\", ""));
                            return filePart.transferTo(new File(pathImage.concat(product.getFile())))
                                    .then(productService.save(product));
                        }))
                .flatMap(product -> ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(Mono.just(product), Product.class))
                .switchIfEmpty(ServerResponse.notFound().build())
                .doOnTerminate(() -> log.info("End - ReactiveEndPoint"));
    }

    public Mono<ServerResponse> createWithImage(ServerRequest serverRequest) {
        log.info("Begin - ReactiveEndPoint");
        Mono<Product> productRequest = serverRequest.multipartData()
                .map(stringPartMultiValueMap -> {
                    FormFieldPart name = (FormFieldPart) stringPartMultiValueMap.toSingleValueMap().get("name");
                    FormFieldPart price = (FormFieldPart) stringPartMultiValueMap.toSingleValueMap().get("price");
                    FormFieldPart idCategory = (FormFieldPart) stringPartMultiValueMap.toSingleValueMap().get("category.id");
                    FormFieldPart nameCategory = (FormFieldPart) stringPartMultiValueMap.toSingleValueMap().get("category.name");
                    Category category = Category.builder()
                            .id(idCategory.value())
                            .name(nameCategory.value())
                            .build();
                    return Product.builder().name(name.value()).price(Double.parseDouble(price.value())).category(category).build();
                });
        return serverRequest.multipartData()
                .map(stringPartMultiValueMap -> stringPartMultiValueMap.toSingleValueMap().get("image-file"))
                .cast(FilePart.class)
                .flatMap(filePart -> productRequest
                        .flatMap(product -> {
                            product.setFile(UUID.randomUUID().toString().concat("_").concat(filePart.filename())
                                    .replace(" ", "_")
                                    .replace(":", "")
                                    .replace("\\", ""));
                            return filePart.transferTo(new File(pathImage.concat(product.getFile())))
                                    .then(productService.save(product));
                        }))
                .flatMap(product -> ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(Mono.just(product), Product.class))
                .doOnTerminate(() -> log.info("End - ReactiveEndPoint"));
    }

}
