package com.luden.api.restfull.service;

import com.luden.api.restfull.model.document.Category;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CategoryService {

    Flux<Category> findAll();
    Mono<Category> findBtId(String id);
    Mono<Category> save(Category category);
}
