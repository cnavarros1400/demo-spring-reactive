package com.luden.api.restfull.repository;

import com.luden.api.restfull.model.document.Category;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface CategoryRepository extends ReactiveMongoRepository<Category, String> {
}
