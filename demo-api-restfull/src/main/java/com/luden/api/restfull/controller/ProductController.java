package com.luden.api.restfull.controller;

import com.luden.api.restfull.model.document.Product;
import com.luden.api.restfull.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.WebExchangeBindException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.io.File;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.*;

@RestController
@RequestMapping("/api/product")
@RequiredArgsConstructor
@Slf4j
public class ProductController {

    private final ProductService productService;

    @Value("${config.uploads.path}")
    private String pathImg;

    @PostMapping("/image/{id}")
    public Mono<ResponseEntity<Product>> uploadImage(@PathVariable String id, @RequestPart FilePart filePart){
        log.info("Begin");
        return productService.findById(id)
                .flatMap(product -> {
                    product.setFile(UUID.randomUUID().toString().concat("_").concat(filePart.filename())
                            .replace(" ","")
                            .replace(":","")
                            .replace("\\",""));
                    return filePart.transferTo(new File(pathImg.concat(product.getFile())))
                            .then(productService.save(product));
                })
                .map(product -> ResponseEntity.ok().body(product))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping
    public Mono<ResponseEntity<Flux<Product>>> list(){
        log.info("Begin");
        return Mono.just(ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(productService.findAll()));
                //.doOnNext();
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<Product>> detail(@PathVariable String id){
        log.info("Begin");
        return productService.findById(id)
                .map(product -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(product))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping
    public Mono<ResponseEntity<Map<String, Object>>> create(@Valid @RequestBody Mono<Product> product){
        log.info("Begin");
        Map<String, Object> response = new HashMap<>();
        return product.flatMap(prod -> {
            return productService.save(prod)
                    .map(p -> {
                        response.put("product", p);
                        response.put("message", "Product registered successfully");
                        response.put("timeStamp", new Date());
                        return ResponseEntity.created(URI.create("/api/product/".concat(p.getId())))
                                .contentType(MediaType.APPLICATION_JSON)
                                .body(response);
                    });
        }).onErrorResume(throwableVar -> {
                    return Mono.just(throwableVar).cast(WebExchangeBindException.class)
                            .flatMap(ex -> Mono.just(ex.getFieldErrors()))
                            .flatMapMany(Flux::fromIterable)
                            .map(fieldError -> "Error en campo ".concat(fieldError.getField())
                                    .concat(" .").concat(Objects.requireNonNull(fieldError.getDefaultMessage())))
                            .collectList()
                            .flatMap(listErrors -> {
                                response.put("errors", listErrors);
                                return Mono.just(ResponseEntity.badRequest().body(response));
                            });
                });

    }

    @PostMapping("/product-with-image")
    public Mono<ResponseEntity<Product>> createWithImage(Product product, @RequestPart FilePart file){
        log.info("Begin");
        //log.info("Request: ".concat(product.toString()));
        product.setFile(UUID.randomUUID().toString().concat("_").concat(file.filename())
                .replace(" ","")
                .replace(":","")
                .replace("\\",""));
        return file.transferTo(new File(pathImg.concat(product.getFile())))
                .then(productService.save(product))
                .map(p -> ResponseEntity.created(URI.create("/api/product/".concat(p.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(p));
    }

    @PutMapping("/{id}")
    public Mono<ResponseEntity<Product>> modify(@PathVariable String id, @RequestBody Product product){
        log.info("Begin");
        log.info("Request: ".concat(product.toString()));
        return productService.findById(id)
                .flatMap(p -> {
                    p.setName(product.getName());
                    p.setPrice(product.getPrice());
                    p.setCategory(product.getCategory());
                    return productService.save(p);
                })
                .map(productModified -> ResponseEntity.ok().body(productModified))
                //.doOnNext(() -> log.info(""))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> delete (@PathVariable String id){
        log.info("Begin");
        return productService.findById(id)
                .flatMap(product -> productService.delete(product)
                        .then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT))))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

}
