package com.luden.api.restfull;

import com.luden.api.restfull.handler.ProductHandler;
import com.luden.api.restfull.service.ProductService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.*;

@Configuration
public class RouterFunctionConfig {

    final ProductService productService;


    public RouterFunctionConfig(ProductService productService) {
        this.productService = productService;
    }

    @Bean
    public RouterFunction<ServerResponse> routes(ProductHandler productHandler){
        return RouterFunctions.route(RequestPredicates.GET("/api/v2/product")
                //.or(RequestPredicates.GET("/api/v3/product")), request -> productHandler.list(request));//replaced by method reference
                .or(RequestPredicates.GET("/api/v3/product")), productHandler::list)
                .andRoute(RequestPredicates.GET("/api/v2/product/{id}")
                        .and(RequestPredicates.contentType(MediaType.APPLICATION_JSON)), productHandler::detail)
                .andRoute(RequestPredicates.POST("/api/v2/product"), productHandler::create)
                .andRoute(RequestPredicates.PUT("/api/v2/product/{id}")
                        .and(RequestPredicates.accept(MediaType.APPLICATION_JSON)), productHandler::modify)
                .andRoute(RequestPredicates.DELETE("/api/v2/product/{id}"), productHandler::delete)
                .andRoute(RequestPredicates.POST("/api/v2/product/image/{id}"), productHandler::uploadImage)
                .andRoute(RequestPredicates.POST("/api/v2/product/product-with-image"), productHandler::createWithImage);
    }

}
