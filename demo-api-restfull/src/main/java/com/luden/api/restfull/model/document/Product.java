package com.luden.api.restfull.model.document;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "product")
public class Product {

    @Id
    private String id;
    @NotEmpty(message = "Field obligatory")
    private String name;
    @NotNull(message = "Field is obligatory")
    @Min(value = 1, message = "Field value higher than 0")
    private Double price;
    //@DateTimeFormat(pattern = "yyyy-MM-dd")
    //private Date createAt;
    @CreatedDate
    private LocalDateTime createAt;
    @Valid
    @NotNull(message = "Specify category.")
    private Category category;
    private String file;

}
