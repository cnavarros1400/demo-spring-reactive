package com.luden.api.restfull.model.document;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotEmpty;

@Document
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Category {
    @Id
    @NotEmpty(message = "Field id category is obligatory")
    private String id;
    @NotEmpty(message = "Field name category is obligatory")
    private String name;
}
