package com.luden.spring.reactor.springbootreactor.model;

import lombok.AllArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class UsuarioConComentarios {
    private User user;
    private Comentario comentario;
}
