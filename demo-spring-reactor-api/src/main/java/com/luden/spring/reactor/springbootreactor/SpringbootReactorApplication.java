package com.luden.spring.reactor.springbootreactor;

import com.luden.spring.reactor.springbootreactor.model.Comentario;
import com.luden.spring.reactor.springbootreactor.model.User;
import com.luden.spring.reactor.springbootreactor.model.UsuarioConComentarios;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;

@Slf4j
@SpringBootApplication
public class SpringbootReactorApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootReactorApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        ejemploMapDoOnNext();
        ejemploIterable();
        ejemploiterable2();
        //FlatMap returns a Flux of different type to the entry
        ejemploFlatMap();
        ejemploConvertirFlujoUsersString();
        ejemploCollectList();
        ejemploCombinarFlujosFlatMap();
        ejemploCombinarFlujosZipWith();
        ejemploCombinarFlujosZipWith2();
        ejemploZipWithRange();
        ejemploInterval();
        delayElements();
        //intervaloInfinito();
        intervaloFnfinito();
        createObservableInterval();
        ejemploContrapresion();
    }

    private void ejemploContrapresion() {
        Flux.range(1, 10)
                .log()
                //.limitRate(2)//alternative method **
                .subscribe(new Subscriber<Integer>() {//**
                    private Subscription subscription;
                    private final Integer limiteRequest = 2;
                    private Integer conumed = 0;
                    @Override
                    public void onSubscribe(Subscription s) {
                        this.subscription = s;
                        this.subscription.request(limiteRequest);
                    }
                    @Override
                    public void onNext(Integer integer) {
                        log.info("OnNext - Overloaded method: ".concat(integer.toString()));
                        conumed++;
                        if (conumed == 2){
                            conumed = 0;
                            this.subscription.request(limiteRequest);
                        }
                    }
                    @Override
                    public void onError(Throwable t) {

                    }
                    @Override
                    public void onComplete() {
                        log.info("OnComplete - overloaded method");
                    }
                });
    }

    private void createObservableInterval() {
        Flux.create(emitter -> {
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                private Integer contador = 0;
                @Override
                public void run() {
                    emitter.next(++contador);
                    if (contador == 10){
                        timer.cancel();
                        emitter.complete();
                    }
                    if(contador == 3){
                        emitter.error(new InterruptedException("Simulated error.. flux on 5"));
                        timer.cancel();
                    }
                }
            },1000, 1000);
        })
                //.doOnNext(next -> log.info(next.toString()))//included on subscribe
                .doOnEach(varDoEach -> log.info("DoOnEach - ".concat(varDoEach.toString())))
                //.doOnComplete(() -> log.info("Flux terminated..."))//included on subscribe
                .subscribe(next -> log.info(next.toString()),
                        error -> log.error(error.getMessage()));
    }

    private void ejemploMapDoOnNext() {
        Flux<String> names = Flux.just("Luxanna Crawnward", "Katarina DuCoteau", "Jaime Duende", "Jhin Khadra", "Bruce Willis", "Bruce Wayne")
                .doOnNext(e -> {
                    log.info("Original flux:");
                    log.info(e);
                })
                .map(String::toUpperCase)
                .doOnNext( e -> log.info("Map flux"));
        names.subscribe(log::info);
    }

    private void intervaloFnfinito() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        Flux.interval(Duration.ofSeconds(1))
                .doOnTerminate(latch::countDown)
                .flatMap(i -> {
                    if (i >= 5){
                        return Flux.error(new InterruptedException("Solo hasta 5"));
                    }else{
                        return Flux.just(i);
                    }
                })
                .map(i -> "hello ".concat(i.toString()))
                .retry(2)
                .subscribe(log::info, e -> log.error(e.getMessage()));
        latch.await();
    }

    private void intervaloInfinito() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        Flux.interval(Duration.ofSeconds(1))
                .doOnTerminate(latch::countDown)

                .map(i -> "hello ".concat(i.toString()))
                .doOnNext(log::info)
                .subscribe();
        latch.await();
    }

    private void delayElements() {
        Flux<Integer> rango = Flux.range(1,12)
                .delayElements(Duration.ofSeconds(1))
                .doOnNext(i -> log.info(i.toString()));
        rango.blockLast();
    }

    private void ejemploInterval() {
        Flux<Integer> rango = Flux.range(1,12);
        Flux<Long> retraso = Flux.interval(Duration.ofSeconds(1));

        rango.zipWith(retraso, (ra, re) -> ra)
                .doOnNext(i -> log.info(i.toString()))
                .blockLast();
    }

    private void ejemploZipWithRange() {
        Flux.just(1,2,3,4)
                .map(i -> (i * 2))
                //flux.range created directly on the argument of zipWith. can be created out of it
                .zipWith(Flux.range(0, 4), (sourceUno, sourceDos) -> String.format("Primer flujo: %d. Segundo flujo %d", sourceUno, sourceDos))
                .subscribe(log::info);
    }

    private void ejemploCombinarFlujosZipWith2() {
        Mono<User> monoUsuario = Mono.fromCallable(() -> new User("Jhon", "Doe"));
        Mono<Comentario> comentariosUserMono   = Mono.fromCallable(() -> {
            Comentario comentario = new Comentario();
            comentario.addComentario("Comment 1 - Jhon doe");
            comentario.addComentario("Comment 2");
            comentario.addComentario("Comment 3");
            return  comentario;
        });
        Mono<UsuarioConComentarios> usuarioConComentariosMono = monoUsuario.zipWith(comentariosUserMono)
                        .map(tuple -> {
                            User u = tuple.getT1();
                            Comentario c = tuple.getT2();
                            return new UsuarioConComentarios(u,c);
                        });
        usuarioConComentariosMono.subscribe(uc -> log.info(uc.toString()));
    }
    private void ejemploCombinarFlujosZipWith() {
        //flujo mono creado a partir de un callable: un metodo
        Mono<User> monoUsuario = Mono.fromCallable(() -> new User("Jhon", "Doe"));
        Mono<Comentario> comentariosUserMono   = Mono.fromCallable(() -> {
            Comentario comentario = new Comentario();
            comentario.addComentario("Comment 1 - Jhon doe");
            comentario.addComentario("Comment 2");
            comentario.addComentario("Comment 3");
            return  comentario;
        });
        Mono<UsuarioConComentarios> usuarioConComentariosMono = monoUsuario.zipWith(comentariosUserMono, UsuarioConComentarios::new);
        usuarioConComentariosMono.subscribe(uc -> log.info(uc.toString()));
    }

    private void ejemploCombinarFlujosFlatMap() {
        //flujo mono creado a partir de un callable: un metodo
        Mono<User> monoUsuario = Mono.fromCallable(() -> new User("Jhon", "Doe"));
        Mono<Comentario> comentariosUserMono   = Mono.fromCallable(() -> {
            Comentario comentario = new Comentario();
            comentario.addComentario("Comment 1 - Jhon doe");
            comentario.addComentario("Comment 2");
                    comentario.addComentario("Comment 3");
            return  comentario;
        });
        monoUsuario.flatMap(u -> comentariosUserMono.map(c -> new UsuarioConComentarios(u, c)))
                .subscribe(uc -> log.info(uc.toString()));
    }

    private void ejemploCollectList() {
        ArrayList<User> usersArray = new ArrayList<>();
        usersArray.add(new User("Carlos", "Navarro"));
        usersArray.add(new User("Svein", "Navarro"));
        usersArray.add(new User("Jaime", "Duende"));


        Flux.fromIterable(usersArray).collectList().subscribe(list -> {
            list.forEach(item -> log.info(item.toString()));
        });
    }

    private void ejemploConvertirFlujoUsersString() {

        ArrayList<User> usersArray = new ArrayList<>();
        usersArray.add(new User("Carlos", "Navarro"));
        usersArray.add(new User("Svein", "Navarro"));
        usersArray.add(new User("Jaime", "Duende"));


        Flux.fromIterable(usersArray).map(user -> user.getName().concat(" ".concat(user.getLastName())))
                .flatMap(user -> {
                    if (user.contains("Navarro")){
                        return Mono.just(user);
                    }else{
                        return Mono.empty();
                    }
                })
                .map(String::toUpperCase).subscribe(user -> {
                    log.info(String.valueOf(user));
                });
    }

    private void ejemploFlatMap() {
        log.info("Inicio ejemplo flatMap");
        Flux<String> names = Flux.just("Luxanna Crawnward", "Katarina DuCoteau", "Jaime Duende", "Jhin Khadra", "Bruce Willis", "Bruce Wayne");

        //Start creating flux from an object: de un arrayList
        ArrayList<String> usersArray = new ArrayList<>();
        names.subscribe(usersArray::add);
        log.info("Items array users: ");
        for (String s : usersArray) {
            log.info(s);
        }

        Flux.fromIterable(usersArray).map(user -> new User(user.split(" ")[0].toUpperCase(), user.split(" ")[1].toUpperCase()))
                .flatMap(user -> {
                    if (user.getName().equalsIgnoreCase("bruce")){
                        return Mono.just(user);
                    }else{
                        return Mono.empty();
                    }
                })
                .map(user -> {
                    String name = user.getName().toLowerCase();
                    String lastName = user.getLastName().toLowerCase();
                    user.setName(name);
                    user.setLastName(lastName);
                    return user;
                }).subscribe(user -> {
                    log.info(String.valueOf(user));
                });
        log.info("End example flatMap");
    }

    public void ejemploiterable2() throws Exception {
        log.info("start example iterable2");
        Flux<String> names = Flux.just("Luxanna Crawnward", "Katarina DuCoteau", "Jaime Duende", "Jhin Khadra", "Bruce Willis", "Bruce Wayne");

        //Start creating flux from an object: de un arrayList
        ArrayList<String> usersArray = new ArrayList<>();
        names.subscribe(usersArray::add);
        log.info("Items array users: ");
        for (String s : usersArray) {
            log.info(s);
        }
        Flux.fromIterable(usersArray).map(user -> new User(user.split(" ")[0].toUpperCase(), user.split(" ")[1].toUpperCase()))
                .filter(user -> user.getName().equalsIgnoreCase("bruce"))
                .map(user -> {
                    String name = user.getName().toLowerCase();
                    String lastName = user.getLastName().toLowerCase();
                    user.setName(name);
                    user.setLastName(lastName);
                    return user;
                }).subscribe(user -> {
            log.info(String.valueOf(user));
        });
        log.info("Fin ejemplo iterable2");
    }

    public void ejemploIterable() throws Exception {
        log.info("Start example iterable");
        Flux<String> names = Flux.just("Luxanna Crawnward", "Katarina DuCoteau", "Jaime Duende", "Jhin Khadra", "Bruce Willis", "Bruce Wayne");

        //Start creating flux from an object: de un arrayList
        ArrayList<String> usersArray = new ArrayList<>();
        names.subscribe(usersArray::add);
        System.out.println("Items array users: ");
        for (String s : usersArray) {
            log.info(s);
        }
        Flux<String> namesFromArray = Flux.fromIterable(usersArray);
        //Start creating flux from an object: de un arrayList
        Flux<User> users = names.map(user -> new User(user.split(" ")[0].toUpperCase(), user.split(" ")[1].toUpperCase()))
                .filter(user -> user.getName().equalsIgnoreCase("bruce"))
                .doOnNext(user -> {
                    if (user == null) {
                        throw new RuntimeException("Throwing exception: Name is empty...");
                    } else {
                        System.out.println(user);
                    }
                })
                .map(user -> {
                    String name = user.getName().toLowerCase();
                    String lastName = user.getLastName().toLowerCase();
                    user.setName(name);
                    user.setLastName(lastName);
                    return user;
                });


        users.subscribe(user -> {
            log.info(String.valueOf(user));
        });
        names.subscribe(log::info,
                error -> log.error(error.getMessage()),
                new Runnable() {
                    @Override
                    public void run() {
                        log.info("Finishing flux execution...");
                    }
                });
        log.info("End example iterable");
    }


}