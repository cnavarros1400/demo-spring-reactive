package com.luden.spring.reactor.springbootreactor.model;

import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@ToString
public class Comentario {
    List<String> comentarios;

    public Comentario(){
        this.comentarios = new ArrayList<>();
    }

    public void addComentario(String comentario){
        this.comentarios.add(comentario);
    }
}
